import React, { Fragment, useState } from "react";

import ToDoForm from "./components/ToDoForm";
import ToDoList from "./components/ToDoList";

import "./App.css";

const App = () => {
  const [todoInput, setTodoInput] = useState("");
  const [todoItems, setTodoItems] = useState([]);
  const [isBeingEdited, setIsBeingEdited] = useState(false);

  const checkIdValue = () => {
    if (todoItems.length !== 0) {
      const lastId = todoItems[todoItems.length - 1].id;
      return lastId + 1;
    }
    return 1;
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (todoInput.trim()) {
      setTodoItems([
        ...todoItems,
        { id: checkIdValue(), text: todoInput, isCompleted: false },
      ]);
    }
    setTodoInput("");
    setIsBeingEdited(false);
  };

  const onMarkedAsDone = (id) => {
    const newItems = todoItems.map((item) =>
      item.id === id ? Object.assign({}, item, { isCompleted: true }) : item
    );
    setTodoItems(newItems);
  };

  const onItemEdit = (id) => {
    const selectedItem = todoItems.find((item) => item.id === id);
    const filteredItems = todoItems.filter((item) => item.id !== id);

    setIsBeingEdited(true);
    setTodoItems(filteredItems);
    setTodoInput(selectedItem.text);
  };

  const onItemDelete = (id) => {
    const filteredItems = todoItems.filter((item) => item.id !== id);

    setTodoItems(filteredItems);
  };

  return (
    <Fragment>
      <h1>Start Doing It</h1>
      <ToDoForm
        value={todoInput}
        checkEdit={isBeingEdited}
        handleChange={(e) => setTodoInput(e.target.value)}
        handleSubmit={(e) => onSubmit(e)}
      />
      <ToDoList
        items={todoItems}
        handleClear={() => setTodoItems([])}
        markAsDone={(id) => onMarkedAsDone(id)}
        handleEdit={(id) => onItemEdit(id)}
        handleDeleteItem={(id) => onItemDelete(id)}
      />
    </Fragment>
  );
};

export default App;
