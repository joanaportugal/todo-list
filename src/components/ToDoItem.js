import React, { Fragment } from "react";

import done from "../assets/right.svg";
import edit from "../assets/pen.svg";
import trash from "../assets/trash.svg";

import "../styles/ToDoItem.css";

const ToDoItem = ({ info, setDone, editItem, deleteItem }) => {
  return (
    <Fragment>
      <article className="todo_item">
        <div className={info.isCompleted ? "done" : ""}>{info.text}</div>
        <div>
          {!info.isCompleted && (
            <button className="item_button" onClick={setDone}>
              <img src={done} alt="done" />
            </button>
          )}
          {!info.isCompleted && (
            <button className="item_button" onClick={editItem}>
              <img src={edit} alt="edit" />
            </button>
          )}
          <button className="item_button" onClick={deleteItem}>
            <img src={trash} alt="delete" />
          </button>
        </div>
      </article>
    </Fragment>
  );
};

export default ToDoItem;
