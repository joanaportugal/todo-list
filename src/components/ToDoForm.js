import React from "react";

import "../styles/ToDoForm.css";

const ToDoForm = ({ value, checkEdit, handleChange, handleSubmit }) => {
  return (
    <form>
      <input
        type="text"
        placeholder="Something I have to do..."
        value={value}
        onChange={handleChange}
      />
      <button onClick={handleSubmit}>{checkEdit ? "EDIT" : "ADD"}</button>
    </form>
  );
};

export default ToDoForm;
