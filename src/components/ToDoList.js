import React from "react";

import ToDoItem from "./ToDoItem";

import "../styles/ToDoList.css";

const ToDoList = ({
  handleClear,
  items,
  markAsDone,
  handleEdit,
  handleDeleteItem
}) => {
  return (
    <div className="todo_list">
      <h2>List</h2>
      {items.length !== 0 && (
        <button className="clear_list" onClick={handleClear}>
          Clear
        </button>
      )}
      <section>
        {items.map((item, index) => (
          <ToDoItem
            key={index}
            info={item}
            setDone={() => markAsDone(item.id)}
            editItem={() => handleEdit(item.id)}
            deleteItem={() => handleDeleteItem(item.id)}
          />
        ))}
      </section>
    </div>
  );
};

export default ToDoList;
